namespace Tests;

public class WheatherForecastTests
{
    [Fact]
    public void TemperatureF_RecordWithParams_ReturnsValidFahrenheitTemperature()
    {
        var forecast = new WeatherForecast(new DateOnly(2000, 1, 1), 0, null);
        var expected = 32;

        Assert.Equal(expected, forecast.TemperatureF);
    }
}